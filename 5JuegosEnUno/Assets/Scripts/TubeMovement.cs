using UnityEngine;

public class TubeMovement : MonoBehaviour
{
    public float tubeSpeed = 2f;
    public float destroyPositionX = -10f;

    private void Update()
    {
        // Mover el tubo hacia la izquierda
        transform.Translate(Vector3.right * tubeSpeed * Time.deltaTime);

        // Verificar si el tubo ha alcanzado la posici�n de destrucci�n
        if (transform.position.x <= destroyPositionX)
        {
            // Destruir el tubo
            Destroy(gameObject);
        }
    }
}
