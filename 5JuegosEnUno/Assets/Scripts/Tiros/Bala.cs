using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bala : MonoBehaviour
{
    public float Velocidad;
    public static bool gameover;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Mundo"))
        {
            transform.SetParent(collision.transform);
        }
        if (collision.gameObject.CompareTag("bala"))
        {
            
            transform.SetParent(collision.transform);
            SceneManager.LoadScene(0);
        }
    }
}
