using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tirador : MonoBehaviour
{
    public Transform Spawn;
    public GameObject Bala;

    void Start()
    {

    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Instantiate(Bala, Spawn.position, Quaternion.identity).GetComponent<Rigidbody>().AddForce(Vector3.right * 100, ForceMode.Impulse);
        }
    }
}