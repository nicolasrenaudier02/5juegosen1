using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cubo_Controller : MonoBehaviour
{
    public Material blueMaterial;
    public Material redMaterial;
    public float forceMagnitude = 5f;

    private Rigidbody rb;
    private bool isBlue;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        isBlue = CompareTag("CuboAzul");

        if (isBlue)
        {
            rb.useGravity = false;

        }
        else
        {
            rb.useGravity = true;
            rb.velocity = Vector3.down;
        }


    }
    private void FixedUpdate()
    {
        if (isBlue)
        {
            rb.AddForce(Vector3.up * forceMagnitude, ForceMode.Force);

        }
        else
        {
            rb.AddForce(Vector3.down * forceMagnitude, ForceMode.Force);
        }
    }

    private void OnMouseDown()
    {
        ChangeColorAndGravity();

    }
    private void ChangeColorAndGravity()
    {
        if (isBlue)
        {
            GetComponent<Renderer>().material = redMaterial;
            rb.velocity = Vector3.down;
            isBlue = false;
            tag = "CuboRojo";
        }
        else
        {
            GetComponent<Renderer>().material = blueMaterial;
            rb.useGravity = false;
            isBlue = true;
            tag = "CuboAzul";
        }
    }
}