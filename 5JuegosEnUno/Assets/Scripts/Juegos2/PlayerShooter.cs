using UnityEngine;

public class PlayerShooter : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform bulletSpawnPoint;
    public float bulletSpeed = 10f;
    public string dianaTag = "Diana";
    public float fireRate = 0.5f; // Tiempo m�nimo entre cada disparo

    private bool canShoot = true;

    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && canShoot)
        {
            ShootBullet();
        }
    }

    private void ShootBullet()
    {
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
        Rigidbody bulletRb = bullet.GetComponent<Rigidbody>();
        bulletRb.velocity = bulletSpawnPoint.forward * bulletSpeed;

        canShoot = false;
        Invoke("EnableShoot", fireRate);
    }

    private void EnableShoot()
    {
        canShoot = true;
    }


}