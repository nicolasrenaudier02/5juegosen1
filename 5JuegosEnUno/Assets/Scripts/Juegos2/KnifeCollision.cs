using UnityEngine;

public class KnifeCollision : MonoBehaviour
{
    private int knifeCollisions = 0;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Knife"))
        {
            knifeCollisions++;

            if (knifeCollisions > 17)
            {
                GameObject player = GameObject.FindGameObjectWithTag("Player");
                if (player != null)
                {
                    Destroy(player);
                }
            }
        }
    }
}

