using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float rotationSpeed = 10f;
    public float damping = 0.5f; // The amount of damping to apply

    private float currentRotationSpeed = 0f;

    private void Update()
    {
        float rotationInput = Input.GetAxis("Horizontal");
        currentRotationSpeed = Mathf.Lerp(currentRotationSpeed, -rotationInput * rotationSpeed, damping);
        transform.Rotate(Vector3.forward, currentRotationSpeed * Time.deltaTime);
    }
}
