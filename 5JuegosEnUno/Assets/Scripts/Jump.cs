using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Jump : MonoBehaviour
{
    public float jumpForce = 10f;
    public float gravityScale = 1f;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        rb.freezeRotation = true;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            rb.useGravity = true;
            rb.velocity = Vector3.up * jumpForce;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            // Destruir el objeto enemigo
            Destroy(this.gameObject);
            // Cambiar a otra escena (reemplaza "NombreDeLaEscena" con el nombre de la escena a la que deseas cambiar)
            SceneManager.LoadScene(0);
        }
    }


}
