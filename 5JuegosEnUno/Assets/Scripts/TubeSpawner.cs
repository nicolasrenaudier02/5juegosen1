using UnityEngine;

public class TubeSpawner : MonoBehaviour
{
    public GameObject[] tubePrefabs;
    public float minSpawnDelay = 1f;
    public float maxSpawnDelay = 3f;
    public float tubeSpeed = 2f;
    public float tubeLifetime = 5f;
    public float destroyPositionX = -10f;

    private void Start()
    {
        StartSpawningTubes();
    }

    private void StartSpawningTubes()
    {
        Invoke("SpawnTube", Random.Range(minSpawnDelay, maxSpawnDelay));
    }

    private void SpawnTube()
    {
        if (tubePrefabs.Length == 0)
        {
            Debug.LogError("No se han configurado prefabs de tubos.");
            return;
        }

        GameObject tubePrefab = tubePrefabs[Random.Range(0, tubePrefabs.Length)];

        GameObject newTube = Instantiate(tubePrefab, transform.position, Quaternion.identity);
        Rigidbody2D tubeRigidbody = newTube.GetComponent<Rigidbody2D>();

        if (tubeRigidbody != null)
        {
            tubeRigidbody.velocity = Vector2.right * tubeSpeed;
        }

        Destroy(newTube, tubeLifetime);
        StartSpawningTubes();
    }
}
